var mongoose = require('mongoose');

var EventsSchema = new mongoose.Schema({
  name: String,
  organizador: String,
  deporte: String,
  img: String,
  place: String,
  latitud: String,
  longitud: String,
  fecha: String,
  hour: String,
  inscritos: Array,
  limit: String,
  desc: String
}, {timestamps: true});

EventsSchema.methods.follow = function(id){
  if(this.inscritos.indexOf(id) === -1){
    this.inscritos.push(id);
  }
  return this.save();
};

EventsSchema.methods.unfollow = function(id){
  this.inscritos.remove(id);
  return this.save();
};

EventsSchema.methods.toJSONFor = function(event){
  return {
    id: this.id,
    name: this.name,
    organizador: this.organizador,
    deporte: this.deporte,
    img: this.img,
    place: this.place,
    latitud: this.latitud,
    longitud: this.longitud,
    fecha: this.fecha,
    hour: this.hour,
    inscritos: this.inscritos,
    limit: this.limit,
    desc: this.desc
  };
};

mongoose.model('Events', EventsSchema);
