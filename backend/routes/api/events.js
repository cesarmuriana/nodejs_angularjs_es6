var router = require('express').Router();
var mongoose = require('mongoose');
var Events = mongoose.model('Events');

// return a list of tags
router.get('/', function(req, res, next) {
    Events.find().then(function(events){
      console.log("Find Events");
    return res.json({events: events});
  }).catch(next);
});

router.get('/:id', function(req, res, next) {
  Events.findById(req.params.id).then(function(events){
    if(!events){ return res.sendStatus(401); }
    return res.json({events: events});
  }).catch(next);
});

router.get('/ftype/:type', function(req, res, next) {
  Events.find({deporte: req.params.type}).then(function(events){
    if(!events){ return res.sendStatus(401);}
    return res.json({events: events});
  }).catch(next);
});

module.exports = router;