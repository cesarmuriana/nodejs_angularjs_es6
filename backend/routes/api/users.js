var mongoose = require('mongoose');
var router = require('express').Router();
var passport = require('passport');
var User = mongoose.model('User');
var auth = require('../auth');
var Events = mongoose.model('Events');
var multer = require('multer');
global.vari = "";



var storage = multer.diskStorage({ //multers disk storage settings
  destination: function (req, file, cb) {
      cb(null,'./uploads/');
      //console.log("Funcio per a guardar la imatge");
  },
  filename: function (req, file, cb) {
      var datetimestamp = Date.now();
      cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1]);
      vari ="uploads/" +file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1];
  }
});     

    //multer settings
    var upload = multer({ 
      storage: storage
    }).single('file');

/*API path that will upload the files*/
    router.post('/media/upload',  auth.required,function(req, res) {
      upload(req,res,function(err){
          if(err){
              res.json({error_code:1,err_desc:err})
              return;
          }

          User.findById(req.payload.id).then(function(user){
            let idUser = user._id;
                User.update({_id: idUser},{$set:{image:vari}}).then(function(){
                        return res.json({user:user,error_code:0,err_desc:err});
                    })
          });  
 });

});



router.get('/user', auth.required, function(req, res, next){
  User.findById(req.payload.id).then(function(user){
    if(!user){ return res.sendStatus(401); }

    return res.json({user: user.toAuthJSON()});
  }).catch(next);
});




router.put('/user', auth.required, function(req, res, next){
  
  User.findById(req.payload.id).then(function(user){
    if(!user){ return res.sendStatus(401); }

    // only update fields that were actually passed...
    if(typeof req.body.user.username !== 'undefined'){
      user.username = req.body.user.username;
    }
    if(typeof req.body.user.email !== 'undefined'){
      user.email = req.body.user.email;
    }
    if(typeof req.body.user.bio !== 'undefined'){
      user.bio = req.body.user.bio;
    }
    // if(typeof req.body.user.image !== 'undefined'){
    //   user.image = req.body.user.image;
    
    // }
    if(typeof req.body.user.password !== 'undefined'){
      user.setPassword(req.body.user.password);
    }

    return user.save().then(function(){
      return res.json({user: user.toAuthJSON()});
    });
  }).catch(next);
});



router.put('/user/subscribe', auth.required, function(req, res, next){
  User.findById(req.payload.id).then(function(user){

  let idUser = user._id;
  let idEvent = req.body.event;

  /* Con el id del usuario lo buscamos y se guarda el evento al que quiere seguir */
  User.update({_id: idUser},{$push:{following:idEvent}}).then(function(){
    
    /* Recoge el evento de base de datos y guarda el id de usuario en inscritos */
    Events.findById(idEvent).then(function(event){
      Events.update({_id: idEvent},{$push:{inscritos:idUser}}).then(function(){
        return res.json({user: user});
      })
    });
  });

  }).catch(next);
});

router.put('/user/unsubscribe', auth.required, function(req, res, next){
  User.findById(req.payload.id).then(function(user){

  let idUser = user._id;
  let idEvent = req.body.event;

  /* Con el id del usuario lo buscamos y se elimina el evento al que quiere dejar de seguir */
  User.update({_id: idUser},{$pull:{following:idEvent}}).then(function(){
    
    /* Recoge el evento de base de datos y elimina el id de usuario en inscritos */
    Events.findById(idEvent).then(function(event){
      Events.update({_id: idEvent},{$pull:{inscritos:idUser}}).then(function(){
        return res.json({user: user});
      })
    });
  });

  }).catch(next);
});

router.post('/user/getuser', auth.required, function(req, res, next){
  User.findById(req.payload.id).then(function(user){
    return res.json({user: user.toProfileJSONFor()});
  }).catch(next);
});

router.post('/user/resolveuser', function(req, res, next){
  User.findById(req.body._id).then(function(user){
    return res.json({user: user.toProfileJSONFor()});
  }).catch(next);
});

router.post('/users/login', function(req, res, next){
  if(!req.body.user.email){
    return res.status(422).json({errors: {email: "can't be blank"}});
  }

  if(!req.body.user.password){
    return res.status(422).json({errors: {password: "can't be blank"}});
  }

  passport.authenticate('local', {session: false}, function(err, user, info){
    if(err){ return next(err); }

    if(user){
      user.token = user.generateJWT();
      return res.json({user: user.toAuthJSON()});
    } else {
      return res.status(422).json(info);
    }
  })(req, res, next);
});


router.post('/users/register', function(req, res, next){
  
  var user = new User();

  user.username = req.body.user.username;
   user.email = req.body.user.email;
 user.setPassword(req.body.user.password);

   user.save().then(function(){
    return res.json({user: user.toAuthJSON()});
   }).catch(next);

});


router.post('/users', function(req, res, next){
  var user = new User();

  user.username = req.body.user.username;
  user.email = req.body.user.email;
  user.setPassword(req.body.user.password);

  user.save().then(function(){
    return res.json({user: user.toAuthJSON()});
  }).catch(next);
});





router.post('/users/sociallogin', function(req, res, next){
 
  let memorystore = req.sessionStore;
  let sessions = memorystore.sessions;
  let sessionUser;
  for(var key in sessions){
    sessionUser = (JSON.parse(sessions[key]).passport.user);
  }

  User.findOne({ '_id' : sessionUser }, function(err, user) {
    if (err)
      return done(err);
    // if the user is found then log them in
    if (user) {
        
        user.token = user.generateJWT();
        return res.json({user: user.toAuthJSON()});// user found, return that user

      } else {
        return res.status(422).json(err);
    }
    });
});



router.get('/auth/googleplus', passport.authenticate('google', { scope: [
  'https://www.googleapis.com/auth/plus.login',
  'https://www.googleapis.com/auth/plus.profile.emails.read'] })
);

router.get('/auth/googleplus/callback',passport.authenticate('google', {
  successRedirect : 'http://127.0.0.1:8081/#!/auth/sociallogin',
  failureRedirect: 'http://127.0.0.1:8081/#!/' }));

  router.get('/auth/github', passport.authenticate('github'));

  router.get('/auth/github/callback',passport.authenticate('github',{
        successRedirect: 'http://127.0.0.1:8081/#!/auth/sociallogin',
        failureRedirect: 'http://127.0.0.1:8081/#!/' }));

module.exports = router;
