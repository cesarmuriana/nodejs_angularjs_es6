function ErrorConfig($stateProvider) {
    'ngInject';
  
    $stateProvider
.state('app.error', {
  url: '/error',

  controllerAs: '$ctrl',
  templateUrl: 'error/error.html',
  title: ' Error',
})
};
export default ErrorConfig;
