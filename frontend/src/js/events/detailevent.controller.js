//import Toaster from "../services/toaster.service";

class DetailEventCtrl {
    constructor(event, $state, organizadorEvento, Toaster, User, AppConstants, Events, $scope, $stateParams, $anchorScroll, JWT, $timeout) {
      'ngInject';

    //console.log(event);
    this.appName = AppConstants.appName;
    this._$scope = $scope;
    this.event = event;
    this.lat = event.latitud;
    this.lng = event.longitud;
    this.mevents;
    $scope.MoreEvents = false;
    this._$state = $state;
    this._toaster = Toaster;
    this._JWT = JWT;
    let idEvent = event._id;
    let suscrito;
    $scope.subscribeButton = organizadorEvento;

    $timeout(function(){
      //console.log(event.mevents);
      let mevents = event.mevents;
      //console.log(mevents[1])
      if (mevents.length != 1){
        $scope.MoreEvents = true;
        
        mevents.filter((events, index) => {
          //console.log(index);
          //console.log(events);
          if (events._id === event._id){
            mevents.splice(index,1);
            //console.log(events);
          }
        });
        
        //console.log(mevents);
        mevents.splice(5);
        $scope.$ctrl.mevents = mevents;
      }
    });
    
    /* Funcion para establecer la orientacion de la flecha del dropdawn*/
    function rotateArrow(position){
      $scope.imgInscritos = {
        webkitTransform: "rotate(" + position + "deg)"
      }
    }

    /* Recoge al organizador y lo pinta */
    User.resolveUser(event.organizador).then(function(value){
      $scope.organizador = "@"+value.username;
    })

    /* "Entra" es un boolean para el dopdawn */
    let entra = true;

    /* Variables utilizadas para el dropdawn */
    let inscritos = event.inscritos;
    let losprimerosRes = [];
    let inscritosRes = [];

    /* Se introduce en una funcion para cuando el usuario se inscriba en el evento 
    se pueda llamar de nuevo*/
    function recogeUsers(){

      /* Recoge el id de todos los usuarios apuntados y resuelve su nombre */
      inscritos.forEach(element => {
        User.resolveUser(element).then(function(value){
          inscritosRes.push("@"+value.username);
        }).then(function(){
          losprimerosRes = inscritosRes.slice(0, 1);
          $scope.inscritos = losprimerosRes;
        });
      });
    }

    recogeUsers();

    $scope.showInscritos = () => {
      if (entra){
        $scope.inscritos = inscritosRes;
        entra = false;
        rotateArrow(180);
      }else{
        $scope.inscritos = losprimerosRes;
        entra = true;
        rotateArrow(0);
      }
    }

    /* Funcion que comprueba si el usuario esta suscrito a dicho evento 
    se hace mediante funcion para poder llamarla despues de suscribirse*/
    function checksuscribe(){
      User.verifyAuth().then(function(value){
        if (value){
          User.getUser().then(function(value){
            if (value.following.length === 0){
              $scope.unorfollow = "Suscribe";
              suscrito = true;
            }else{
              value.following.forEach(element =>{
                if (element === idEvent){
                  $scope.unorfollow = "Unsuscribe";
                  suscrito = false;
                }else{
                  $scope.unorfollow = "Suscribe";
                  suscrito = true;
                }
              })
            }
          });
        }else{
          $scope.unorfollow = "Suscribe";
          suscrito = true;
        }
      });
    };

    checksuscribe();


    function unsuscribe(){
      User.userUnsuscribe(idEvent).then(function(){
        $scope.unorfollow = "Suscribe";
        suscrito = true;
        let usuario = "@"+User.current.username;

        inscritosRes.filter((element, index) => {
          if (element === usuario){
            inscritosRes.splice(index,1);
          }
        })
      }).then(function(){
        entra = false;
        rotateArrow(180);
        $scope.inscritos = inscritosRes;
      });
    }

    $scope.joinup = () => {

      User.verifyAuth().then((value) => {
        if (!value){
            this._toaster.showToaster('error','Debe estar logeado para apuntarse');
            this._$state.go('app.login');
        }else{
          /* Si el usuario esta logueado puede suscribirse */
          /* Suscrito esta comprobando si el usuario está o no suscrito al evento, si no lo esta
          entrara para suscribirse y si lo esta irà a la función de unsuscribe */
          if (suscrito){
            User.userSuscribe(idEvent).then((value)=>{
              inscritosRes.push("@"+value.data.user.username);
              $scope.inscritos = inscritosRes;
              entra = false;
              rotateArrow(180);
            });
            $scope.unorfollow = "Unsuscribe";
            suscrito = false;
          }else{
            unsuscribe();
          }
        }
      });
    }
  }
}
  
  export default DetailEventCtrl;