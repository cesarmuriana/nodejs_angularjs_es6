class AppHeaderCtrl {
  constructor(AppConstants, User, $scope) {
    'ngInject';

    this.appName = AppConstants.appName;
    this.currentUser = User.current;
    var vm = this;
    // $scope.$watch('User.current', (newUser) => {
    //   this.currentUser = newUser;
    // })

    $scope.$watch('User.current', (newUser) => {
      // console.log("newUser header component", newUser); 
      vm.user = newUser;
      if (vm.user != null) {
      if (vm.user.media == "https://robohash.org/") {
      vm.user.media = vm.user.media + vm.user.name;
      }
      }
      /* vm._JWT.decodeToken().then(
      (res) => { 
      vm.currentUser = res;
      vm.user = res;
      console.log("-------user-----", res );
      }) */
      })

  }
}

let AppHeader = {
  controller: AppHeaderCtrl,
  templateUrl: 'layout/header.html'
};

export default AppHeader;
