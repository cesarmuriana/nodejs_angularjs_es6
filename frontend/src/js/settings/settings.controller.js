class SettingsCtrl {
  constructor(User,$state,Upload,Toaster) {
    'ngInject';

    this.currentUser = User.current;
    this._User = User;
    this._$state = $state;
    
  
    this.formData = {
      email: User.current.email,
      bio: User.current.bio,
      image: User.current.image,
      username: User.current.username
    }
    this.logout = User.logout.bind(User);
    var vm = this;
   



    
 vm.submitForm=function() {
    if (vm.setting_form.file.$valid && vm.setting_form.file) { //check if from is valid
      vm.upload(vm.file); //call upload function
  
    }
    this.isSubmitting = true;
    this._User.update(this.formData).then(
      (user) => {
      },
      (err) => {
        this.isSubmitting = false;
        this.errors = err.data.errors;
      }
    )
}


  

vm.upload = function (file) {
  this._toaster = Toaster;
  User.updateImg(file).then(function (resp) { //upload function returns a promise
  
      if(resp){
        location.reload(true);
          vm._toaster.showToaster('success','Successfully image upload');   
          vm._$state.go('app.home');
         
          
     } else {
         vm._toaster.showToaster('error','Error image upload');
         vm._$state.go('app.home');
          
 }

      
  }, function (evt) { 
      var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
      vm.progress = 'progress: ' + progressPercentage + '% '; // capture upload progress
  });
  };
}


}

export default SettingsCtrl;
