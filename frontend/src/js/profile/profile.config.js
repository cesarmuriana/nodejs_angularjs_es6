function ProfileConfig($stateProvider) {
  'ngInject';

  $stateProvider
  .state('app.profile', {
    url: '/@:username',
    controller: 'ProfileCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'profile/profile.html',
    resolve: {
      profile: function(Profile, $state, $stateParams, Events) {
        return Profile.get($stateParams.username).then(
          ((profile) => {
              //console.log(profile);
              let myevents = [];
              let followevents = [];
              return Events.getAll().then((events) => {    
                  profile.created.map((myevent) => {
                      events.filter((event) => {
                        if (event._id === myevent) {
                            myevents.push(event);
                        }
                      })
                  });

                  profile.following.map((fevent) => {
                      events.filter((event) => {
                          if (event._id === fevent) {
                              followevents.push(event);
                          }
                      })
                });
                profile.myevents = myevents;
                profile.followevents = followevents;
                return profile;
              })
              
          }),
          (err) => $state.go('app.home')
        )
      }
    }

  })
};

export default ProfileConfig;
