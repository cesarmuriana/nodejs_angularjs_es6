function HomeConfig($stateProvider) {
  'ngInject';

  $stateProvider
  .state('app.home', {
    url: '/',
    controller: 'HomeCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'home/home.html',
    title: 'Home' ,
    resolve: {
      profile: function(User, $state, $stateParams, Events) {
        console.log(User.current);
        return User.verifyAuth().then((value) => {
          if (value){
           return User.getUser().then(
            ((profile) => {
              //  console.log(res);
              //  console.log(res.data)
                console.log(profile);
              
                let followevents = [];
                return Events.getAll().then((events) => {    
                    profile.following.map((fevent) => {
                        events.filter((event) => {
                            if (event._id === fevent) {
                                followevents.push(event);
                            }
                        })
                  });
                 
                  profile.followevents = followevents;
                  
                  return profile;
                })
                
            }),
            (err) => {
              return undefined;
            }
          )
        }else{
          return undefined;
        }
      });
      }
    }
  });

};

export default HomeConfig;
