export default class User {
  constructor(JWT, AppConstants, $http, $state, $q, Upload) {
    'ngInject';

    this._JWT = JWT;
    this._AppConstants = AppConstants;
    this._$http = $http;
    this._$state = $state;
    this._$q = $q;
    this._Upload= Upload;
    this.current = null;

  }


  attemptAuth(type, credentials) {
    let route = '/'+ type;
    return this._$http({
      url: this._AppConstants.api + '/users' + route,
      method: 'POST',
      data: {
        user: credentials
      }
    }).then(
      (res) => {
        this._JWT.save(res.data.user.token);
        this.current = res.data.user;

        return res;
      }
    );
  }

  update(fields) {
    return this._$http({
      url:  this._AppConstants.api + '/user',
      method: 'PUT',
      data: { user: fields }
    }).then(
      (res) => {
        this.current = res.data.user;
        return res.data.user;
      }
    )
  }


//Upload.upload
 updateImg(file) {
  return this._Upload.upload({
    url:  this._AppConstants.api + '/media/upload',
    method: 'POST',
    headers: {
      Authorization: 'Token ' + this._JWT.get()
    },
    data:{file:file} 
  }).then(
    (res) => {
      this.current = null;
      this._$state.go(this._$state.$current, null, { reload: true });
      this.current = res.data.user;
      return res.data.user;
    }
  )
}



  logout() {
    this.current = null;
    this._JWT.destroy();
    this._$state.go(this._$state.$current, null, { reload: true });
  }

  userSuscribe(idEvent){
      return this._$http({
        url: this._AppConstants.api + '/user/subscribe',
        method: 'PUT',
        headers: {
          Authorization: 'Token ' + this._JWT.get()
        },
        data: {
          event: idEvent
        }
      })
    }
    userUnsuscribe(idEvent){
      return this._$http({
        url: this._AppConstants.api + '/user/unsubscribe',
        method: 'PUT',
        headers: {
          Authorization: 'Token ' + this._JWT.get()
        },
        data: {
          event: idEvent
        }
      }).then(function(value){
        return value.data.user;
      })
    }
    
  resolveUser(id){
    return this._$http({
      url: this._AppConstants.api + '/user/resolveuser',
      method: 'POST',
      data: {
        _id: id
      }
    }).then(function(value){
      return value.data.user;
    })
  }

  getUser(){
    return this._$http({
      url: this._AppConstants.api + '/user/getuser',
      method: 'POST',
      headers: {
        Authorization: 'Token ' + this._JWT.get()
      }
    }).then(function(value){
      return value.data.user;
    })
  }

  verifyAuth() {
    let deferred = this._$q.defer();

    // check for JWT token
    if (!this._JWT.get()) {
      deferred.resolve(false);
      return deferred.promise;
    }

    if (this.current) {
      deferred.resolve(true);

    } else {
      this._$http({
        url: this._AppConstants.api + '/user',
        method: 'GET',
        headers: {
          Authorization: 'Token ' + this._JWT.get()
        }
      }).then(
        (res) => {
          this.current = res.data.user;
          deferred.resolve(true);
        },

        (err) => {
          this._JWT.destroy();
          deferred.resolve(false);
        }
      )
    }

    return deferred.promise;
  }


  ensureAuthIs(bool) {
    let deferred = this._$q.defer();

    this.verifyAuth().then((authValid) => {
      if (authValid !== bool) {
        this._$state.go('app.home')
        deferred.resolve(false);
      } else {
        deferred.resolve(true);
      }

    });

    return deferred.promise;
  }

}
