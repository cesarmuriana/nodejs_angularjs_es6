import angular from 'angular';

// Create the module where our functionality can attach to
let neweventsModule = angular.module('app.newevents', []);

// Include our UI-Router config settings
import NeweventsConfig from './newevents.config';
neweventsModule.config(NeweventsConfig);


// Controllers
import NeweventsCtrl from './newevents.controller';
neweventsModule.controller('NeweventsCtrl',NeweventsCtrl);


export default neweventsModule;
