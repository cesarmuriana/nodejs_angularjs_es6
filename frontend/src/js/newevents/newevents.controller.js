class NeweventsCtrl {
    constructor(user, User, Newevents, $state, $scope, Toaster) {
      'ngInject';
      this._$state = $state;
      this._$scope = $scope;
      this._Newevents = Newevents;
      this._User = User.getUser(); /*If we use this, we need to use ,then because it returns like a promise*/
      this.user = user;
      this.Toaster = Toaster;

    }

    submit() {
      //console.log(this.user);
      
      this.isSubmitting = true;
      let newevent = this.newevent;
      /* Cambiar en value a mayuscules i ferles asi en minuscules en bd es mayus */
      if (newevent.deporte )
      newevent.img = 'images/'+newevent.deporte+'.jpg';
      if (newevent.deporte == 'Basquet')
          newevent.img = 'images/'+newevent.deporte+'.jpeg';
      newevent.organizador = this.user.id;
      newevent.inscritos = [];
      newevent.inscritos.push(this.user.id);

      console.log(newevent);

      this._Newevents.save(newevent).then((event) => {
        console.log(event);
        if (event.status == 200) {
          this.Toaster.showToaster('success','Evento creado correctamente');
          this._$state.go('app.home');
        }else {
          this.Toaster.showToaster('error','Ha habido algun error al crear el evento');
          this._$state.go('app.home');
        }
      });
    }
  }
  
  export default NeweventsCtrl;
  